import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-product-order-complete',
  templateUrl: './product-order-complete.component.html',
  styleUrls: ['./product-order-complete.component.css']
})
export class ProductOrderCompleteComponent implements OnInit {

  constructor() {
    localStorage.removeItem('basket');
  }

  ngOnInit() {
  }

}
