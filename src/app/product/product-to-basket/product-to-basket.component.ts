import {Component, Input, OnInit} from '@angular/core';
import {ProductModel} from '../product.model';

@Component({
  selector: 'app-product-to-basket',
  templateUrl: './product-to-basket.component.html',
  styleUrls: ['./product-to-basket.component.css']
})
export class ProductToBasketComponent implements OnInit {
  @Input() public product: ProductModel;
  private basket: ProductModel[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  addToBasket() {
    if (localStorage.getItem('basket') !== null) {
      this.basket = JSON.parse(localStorage.getItem('basket'));
    } else {
      localStorage.setItem('basket', '[]');
    }

    this.basket.push(this.product);

    localStorage.setItem('basket', JSON.stringify(this.basket));
  }
}
