import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductToBasketComponent} from './product-to-basket.component';

describe('ProductToBasketComponent', () => {
  let component: ProductToBasketComponent;
  let fixture: ComponentFixture<ProductToBasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductToBasketComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductToBasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
