import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProductModel} from '../product.model';
import {ProductService} from '../product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  private product: ProductModel;
  private id: number;


  constructor(private router: Router, private productService: ProductService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params.id;
        if (this.id) {
          this.productService.getProductById(this.id)
            .subscribe(result => this.product = result);
        } else {
          this.product = new ProductModel(null, '', '', 0, 0.00, false, '');
        }
      }
    );
  }

  submitProduct() {
    if (this.id) {
      this.productService.putProduct(this.product).subscribe();
      this.router.navigate(['/product/' + this.id]);
    } else {
      this.productService.addProduct(this.product).subscribe();
      this.router.navigate(['/product']);
    }
  }

  onCancel() {
    if (this.id) {
      this.router.navigate(['/product/' + this.id]);
    } else {
      this.router.navigate(['/product']);
    }
  }

}
