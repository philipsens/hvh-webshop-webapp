import {Component, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProductModel} from '../product.model';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  private id: number;
  public product: ProductModel;

  constructor(private productService: ProductService, private auth: AuthService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params.id;
        this.productService.getProductById(this.id)
          .subscribe(result => this.product = result);
      }
    );
  }

}
