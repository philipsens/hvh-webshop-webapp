import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-product-order',
  templateUrl: './product-order.component.html',
  styleUrls: ['./product-order.component.css']
})
export class ProductOrderComponent implements OnInit {
  name: string;
  adres: string;
  city: string;

  constructor() {
  }

  ngOnInit() {
  }

}
