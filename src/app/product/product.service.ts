import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductModel} from './product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  /** Base url for the product API */
  private productUrl = '/api/product';

  constructor(private http: HttpClient) {
  }

  /** GET products from the server */
  getProduct(): Observable<ProductModel[]> {
    return this.http.get<ProductModel[]>(this.productUrl).pipe();
  }

  /** GET products from the server */
  getProductById(id: number): Observable<ProductModel> {
    return this.http.get<ProductModel>(this.productUrl + '/' + id).pipe();
  }

  /** POST: add a new product to the database */
  addProduct(product: ProductModel): Observable<ProductModel> {
    /**
     * This is the only way to set the responsetype to text as far as i know.
     * This implementation creates a warning in IntelliJ although it fixes the error. For this reason im ignoring it.
     */
    // @ts-ignore
    return this.http.post<ProductModel>(this.productUrl, product, {responseType: 'text'});
  }

  /** PUT: update an existing product in the database */
  putProduct(product: ProductModel): Observable<ProductModel> {
    // @ts-ignore
    return this.http.put<ProductModel>(this.productUrl, product, {responseType: 'text'}).pipe();
  }

  /** POST: delete an product from the database */
  deleteProduct(id: number): Observable<any> {
    // @ts-ignore
    return this.http.delete<any>(this.productUrl + '/' + id, {responseType: 'text'}).pipe();
  }
}
