import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../product.model';
import {AuthService} from '../../auth.service';
import {ProductService} from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  public products: ProductModel[] = [];

  constructor(private productService: ProductService, public auth: AuthService) {
  }

  ngOnInit() {
    this.productService.getProduct().subscribe(result => this.products = result);
  }

  deleteProduct(product: ProductModel) {
    if (window.confirm('Weet u zeker dat u het product ' + product.name + ' permanent wilt verwijderen?')) {
      this.productService.deleteProduct(product.id).subscribe();
      this.productService.getProduct().subscribe(result => this.products = result);
    }
  }
}
