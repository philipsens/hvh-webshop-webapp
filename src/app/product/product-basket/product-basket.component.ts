import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../product.model';

@Component({
  selector: 'app-product-basket',
  templateUrl: './product-basket.component.html',
  styleUrls: ['./product-basket.component.css']
})
export class ProductBasketComponent implements OnInit {
  public products: ProductModel[] = [];
  private total: number;

  constructor() {
  }

  ngOnInit() {
    this.getBasket();
    this.calculateTotal();
  }

  private getBasket() {
    if (localStorage.getItem('basket') !== null) {
      this.products = JSON.parse(localStorage.getItem('basket'));
    }
  }

  private calculateTotal() {
    if (this.products.length > 0) {
      this.total = Math.round(this.products
        .map(product => product.price)
        .reduce((sum, current) => sum + current) * 100) / 100;
    }
  }

  deleteProduct(i: number) {
    this.products.splice(i, 1);
    this.calculateTotal();
    localStorage.setItem('basket', JSON.stringify(this.products));
  }
}
