export class ProductModel {
  id: number;
  name: string;
  description: string;
  quantity: number;
  price: number;
  custom: boolean;
  image: string;

  constructor(id: number, name: string, description: string, quantity: number, price: number, custom: boolean, image: string) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.quantity = quantity;
    this.price = price;
    this.custom = custom;
    this.image = image;
  }
}
