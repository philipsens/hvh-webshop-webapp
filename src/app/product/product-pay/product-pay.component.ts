import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../product.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-pay',
  templateUrl: './product-pay.component.html',
  styleUrls: ['./product-pay.component.css']
})
export class ProductPayComponent implements OnInit {
  public products: ProductModel[] = [];
  private total: number;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.getBasket();
    this.calculateTotal();
  }

  private getBasket() {
    if (localStorage.getItem('basket') !== null) {
      this.products = JSON.parse(localStorage.getItem('basket'));
    }
  }

  private calculateTotal() {
    this.total = Math.round(this.products
      .map(product => product.price)
      .reduce((sum, current) => sum + current) * 100) / 100;
  }

  pay() {
    window.open('https://bunq.me/sergi/' + this.total);
    this.router.navigateByUrl('/bestelling-compleet');
  }
}
