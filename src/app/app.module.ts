import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {ProductComponent} from './product/product.component';
import {ProductListComponent} from './product/product-list/product-list.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ProductDetailComponent} from './product/product-detail/product-detail.component';
import {NavigationComponent} from './navigation/navigation.component';
import {ProductToBasketComponent} from './product/product-to-basket/product-to-basket.component';
import {ProductBasketComponent} from './product/product-basket/product-basket.component';
import {ProductOrderComponent} from './product/product-order/product-order.component';
import {LoginComponent} from './login/login.component';
import {FormsModule} from '@angular/forms';
import {AuthInterceptor} from './auth.interceptor';
import {ProductFormComponent} from './product/product-form/product-form.component';
import {ProductPayComponent} from './product/product-pay/product-pay.component';
import {ProductOrderCompleteComponent} from './product/product-order-complete/product-order-complete.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductComponent,
    ProductListComponent,
    ProductDetailComponent,
    NavigationComponent,
    ProductToBasketComponent,
    ProductBasketComponent,
    ProductOrderComponent,
    LoginComponent,
    ProductFormComponent,
    ProductPayComponent,
    ProductOrderCompleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
