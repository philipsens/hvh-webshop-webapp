import {Component, OnInit} from '@angular/core';
import {User} from '../User';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: User;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.user = new User();
  }

  onSubmit() {
    this.authService.loginUser(this.user);
  }
}
