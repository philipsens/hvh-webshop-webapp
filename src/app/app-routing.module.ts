import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ProductComponent} from './product/product.component';
import {ProductDetailComponent} from './product/product-detail/product-detail.component';
import {ProductBasketComponent} from './product/product-basket/product-basket.component';
import {ProductOrderComponent} from './product/product-order/product-order.component';
import {AuthGuardService} from './auth-guard.service';
import {LoginComponent} from './login/login.component';
import {ProductFormComponent} from './product/product-form/product-form.component';
import {ProductPayComponent} from './product/product-pay/product-pay.component';
import {ProductOrderCompleteComponent} from './product/product-order-complete/product-order-complete.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'product', component: ProductComponent},
  {path: 'product/nieuw', component: ProductFormComponent, canActivate: [AuthGuardService]},
  {path: 'product/:id', component: ProductDetailComponent},
  {path: 'product/:id/bewerk', component: ProductFormComponent, canActivate: [AuthGuardService]},
  {path: 'winkelmand', component: ProductBasketComponent},
  {path: 'bestel', component: ProductOrderComponent, canActivate: [AuthGuardService]},
  {path: 'betaal', component: ProductPayComponent, canActivate: [AuthGuardService]},
  {path: 'bestelling-compleet', component: ProductOrderCompleteComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
