import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './User';
import {Router} from '@angular/router';

/**
 * The Auth Service.
 *
 * Service for authorization.
 *
 * @author Sergi Philipsen
 */

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userURL = '/api/login';
  private permanent: boolean;

  constructor(private http: HttpClient, private router: Router) {
  }

  public getToken() {
    return localStorage.getItem('authToken');
  }

  private setToken(user: User, permanent: boolean = false) {
    this.permanent = permanent;
    return localStorage.setItem('authToken', btoa(`${user.username}:${user.password}`));
  }

  public delToken() {
    return localStorage.removeItem('authToken');
  }

  public isAuthenticated() {
    return !(this.getToken() === null);
  }

  public loginUser(user: User) {
    this.http.post<User>(this.userURL, user).pipe().subscribe(result => {
      if (user.username === result.username) {
        this.setToken(user, true);
        this.setRole(result.role);
        this.router.navigate(['home']);
      }
    });
  }

  public logout() {
    this.delToken();
    this.delRole();
  }

  private setRole(role: string) {
    return localStorage.setItem('role', role);
  }

  public getRole() {
    return localStorage.getItem('role');

  }

  public delRole() {
    return localStorage.removeItem('role');
  }
}
